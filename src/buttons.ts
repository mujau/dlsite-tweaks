import { apiButton } from "./components/apiButton.ts";
import { asButton } from "./components/asButton.ts";
import { titleCopyButton } from "./components/titleCopyButton.ts";
import { getMeta } from "./metadata.ts";
import { getElements } from "./utils/getElements.ts";

export function addButtons(target: HTMLElement): void {
  const { site, title, maker, code, is_ana } = getMeta();
  const buttons = [
    apiButton(site, code, is_ana),
    titleCopyButton(code, maker!, title!),
  ];
  if (!is_ana) buttons.unshift(asButton(code));
  target.after(...buttons);
}

export function addApiButtonToErrorPage(): void {
  const { site, code, is_ana } = getMeta();
  const { wrapper } = getElements({ wrapper: "#wrapper" });
  wrapper.prepend(apiButton(site, code, is_ana));
}
