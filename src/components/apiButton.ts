import { buildProductQueryURL } from "../lib/DLsite/product.ts";
import { Site } from "../lib/DLsite/types.ts";
import { i18n } from "../strings.ts";
import { html } from "../utils/html.ts";

export function apiButton(
  site: Site,
  code: string,
  is_ana: boolean,
): HTMLElement {
  const link = buildProductQueryURL(code, {
    locale: "",
    site,
    ana: is_ana,
  });
  const button = html`
    <div id="無-dltweak__api-button" class="無-dltweak__button">
      <a href=${link} rel="noreferrer" target="_blank">
        <span class="button_label">${i18n.api_button__label}</span>
      </a>
    </div>
  ` as HTMLDivElement;
  return button;
}
