import type {} from "typed-query-selector";
import { addRuby } from "./addRuby.ts";
import { addButtons } from "./buttons.ts";
import { handleErrorPage } from "./error.ts";
import { addFileInfo } from "./fileInfo.ts";
import { removeWebP } from "./removeWebP.ts";
import { restoreTags } from "./restoreTags.ts";
import { registerSettings, settings } from "./settings.ts";
import style from "./style.css";
import { unmaskWords } from "./unmaskWords.ts";
import { getElements } from "./utils/getElements.ts";

function isErrorPage() {
  return !!document.querySelector(".error_box");
}

void function init() {
  GM_addStyle(style);
  if (isErrorPage()) {
    handleErrorPage();
    return;
  }
  registerSettings();
  const { workName } = getElements({ workName: "h1#work_name" });
  if (settings.removeWebP) removeWebP();
  if (settings.restoreTags) restoreTags();
  if (settings.unmaskWords) unmaskWords(workName);
  addButtons(workName);
  addRuby(workName);
  addFileInfo();
}();
