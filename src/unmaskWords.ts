import { settings } from "./settings.ts";
import { maskedWords } from "./strings.ts";
import { getElements } from "./utils/getElements.ts";
import { html } from "./utils/html.ts";
import { replaceText } from "./utils/replaceText.ts";

const enabled = settings.unmaskWords || undefined;

const re =
  enabled &&
  new RegExp(
    `(${Object.keys(maskedWords)
      .map((m) => m.replaceAll("*", "\\*"))
      .join(")|(")})`,
    "gi",
  );
const maskRe = /○|\*/;

const maskedCharByIndex = Object.values(maskedWords);

function unmaskedWordFromGroups(...arr: string[]) {
  const word = arr.shift()!;
  const char = maskedCharByIndex[arr.findIndex((e) => !!e)]!;
  /** No Japanese masking or lowercase letters. */
  const capitalize = /^[^○\p{Ll}]+$/u.test(word);
  return word.replace(maskRe, capitalize ? char.toUpperCase() : char);
}

function unmask(target: HTMLElement) {
  replaceText(target, re!, (arr) => {
    return html`<span class="無-dltweak__unmasked-word" title=${arr[0]}
      >${unmaskedWordFromGroups(...arr)}</span
    >` as Element;
  });
}

export function unmaskWords(workName: HTMLElement): void {
  const { workParts, series } = getElements({
    workParts: ".work_parts_container",
    series: [document, "a[href*='title_d']", true],
  });
  if (maskRe.test(workName.textContent!)) {
    unmask(workName);
    document.title = document.title.replaceAll(re!, unmaskedWordFromGroups);
  }
  if (maskRe.test(workParts.textContent!)) {
    unmask(workParts);
  }
  if (series && maskRe.test(series.textContent!)) {
    unmask(series);
  }
}
