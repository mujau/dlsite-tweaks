import json from "@rollup/plugin-json";
import resolve from "@rollup/plugin-node-resolve";
import strip from "@rollup/plugin-strip";
import typescript from "@rollup/plugin-typescript";
import { defineConfig } from "rollup";
import css from "rollup-plugin-import-css";
import macros from "unplugin-macros/rollup";
import meta from "./meta.ts";
import pkg from "./package.json" with { type: "json" };

process.env.build ??= "prod";
const plugins = [
  resolve({ browser: true }),
  json({ namedExports: false }),
  css({ minify: true }),
  macros(),
  typescript(),
];

if (process.env.build !== "dev") {
  plugins.push(
    strip({
      include: "**/*.(js|ts)",
      functions: ["console.debug"],
      labels: ["strip"],
    }),
  );
}

export default defineConfig({
  input: "./src/index.ts",
  output: {
    file: pkg.main,
    format: "iife",
    banner: meta,
    sourcemap: false,
    generatedCode: "es2015",
    externalLiveBindings: false,
    esModule: false,
    interop: "esModule",
    indent: false,
  },
  plugins,
});
