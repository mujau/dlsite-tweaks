import { workLinkRe } from "./lib/DLsite/code.ts";
import { buildProductQueryURL } from "./lib/DLsite/product.ts";
import {
  Product,
  Site,
  WorkCode,
  WorkCodeLinkGroups,
} from "./lib/DLsite/types.ts";
import { getElements } from "./utils/getElements.ts";

export interface Meta {
  site: Site;
  code: WorkCode;
  is_ana: boolean;
  title: string | undefined;
  maker: string | undefined;
}

export type MetaFromAPI = Pick<Product, "work_name_kana" | "contents">;

let meta: Meta;
let metaFromAPI: MetaFromAPI;

export function getMeta(): Meta {
  if (meta) return meta;

  const match = location.href.match(workLinkRe);
  if (!match) {
    throw new Error("The page URL couldn't be matched");
  }
  const { site, ana, work_code } = match.groups as WorkCodeLinkGroups;
  const { title, makerName } = getElements(
    {
      title: "h1#work_name",
      makerName: "span.maker_name",
    },
    false,
  );
  return (meta = {
    site: site as Site,
    code: work_code!,
    is_ana: ana === "announce",
    title: title?.textContent!.trim(), // https://github.com/microsoft/TypeScript/issues/10315
    maker: makerName?.textContent!.trim(),
  });
}

const fields = ["work_name_kana", "contents"] as const;

export async function getProduct(): Promise<MetaFromAPI> {
  const { code, site, is_ana } = meta as Required<Meta>;
  const headers = { "X-Requested-With": "XMLHttpRequest" };
  const query = buildProductQueryURL(code, {
    fields,
    site,
    ana: is_ana,
  });

  const response = await fetch(query, { headers });
  if (!response.ok) {
    console.debug(response);
    throw new Error("Product metadata fetch failed");
  }
  const json = await response.json();
  if (!Array.isArray(json)) {
    console.debug("Not an array: %o", json);
    throw new Error("API response is expected to be an array");
  }
  if (!json.length) {
    console.debug("Empty response from %s", query);
    throw new Error("Empty API response");
  }
  if (json.length !== 1) {
    console.debug("More than one result: %o", json);
    throw new Error("The API returned more than one result");
  }
  const product = json[0];
  return product as Product;
}

export async function getMetaFromAPI(): Promise<MetaFromAPI> {
  if (metaFromAPI) return metaFromAPI;

  console.debug("Fetching metadata");
  return (metaFromAPI = await getProduct());
}
