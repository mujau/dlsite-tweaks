import type { ValueOf } from "../@types/utils.ts";
import {
  MenuCommand,
  registerMenuCommand,
  registerMenuCommands,
} from "./utils/menuCommands.ts";

interface Settings {
  removeWebP: boolean;
  restoreTags: boolean;
  unmaskWords: boolean;
  unmaskWords_highlight: boolean;
  titleFormat: string;
  katakanaToHiragana: boolean;
}

const KEYS = {
  removeWebP: "REMOVE_WEBP",
  restoreTags: "RESTORE_TAGS",
  unmaskWords: "UNMASK_WORDS",
  unmaskWords_highlight: "UNMASK_WORDS__HIGHLIGHT",
  titleFormat: "TITLE_FORMAT",
  katakanaToHiragana: "KATAKANA_TO_HIRAGANA",
} as const;

type CommandID = ValueOf<typeof KEYS>;

export const settings: Settings = {
  removeWebP: GM_getValue(KEYS.removeWebP, true),
  restoreTags: GM_getValue(KEYS.restoreTags, true),
  unmaskWords: GM_getValue(KEYS.unmaskWords, true),
  unmaskWords_highlight: GM_getValue(KEYS.unmaskWords_highlight, true),
  titleFormat: GM_getValue(KEYS.titleFormat, "[$code] [$maker] $title"),
  katakanaToHiragana: GM_getValue(KEYS.katakanaToHiragana, false),
};

const commands: MenuCommand<CommandID>[] = [
  {
    id: KEYS.removeWebP,
    get caption() {
      return `Remove WebP [${settings.removeWebP ? "enabled" : "disabled"}]`;
    },
    onClick() {
      settings.removeWebP = !settings.removeWebP;
      GM_setValue(KEYS.removeWebP, settings.removeWebP);
      registerMenuCommand(this);
    },
  },
  {
    id: KEYS.restoreTags,
    get caption() {
      return `Restore Tags [${settings.restoreTags ? "enabled" : "disabled"}]`;
    },
    onClick() {
      settings.restoreTags = !settings.restoreTags;
      GM_setValue(KEYS.restoreTags, settings.restoreTags);
      registerMenuCommand(this);
    },
  },
  {
    id: KEYS.unmaskWords,
    get caption() {
      return `Unmask censored words [${
        settings.unmaskWords ? "enabled" : "disabled"
      }]`;
    },
    onClick() {
      settings.unmaskWords = !settings.unmaskWords;
      GM_setValue(KEYS.unmaskWords, settings.unmaskWords);
      // Full rerender as unmaskWords_highlight depends on it.
      registerMenuCommands(commands);
    },
  },
  {
    id: KEYS.unmaskWords_highlight,
    get caption() {
      return `Highlight unmasked words [${
        settings.unmaskWords_highlight ? "enabled" : "disabled"
      }]`;
    },
    onClick() {
      settings.unmaskWords_highlight = !settings.unmaskWords_highlight;
      GM_setValue(KEYS.unmaskWords_highlight, settings.unmaskWords_highlight);
      registerMenuCommand(this);
    },
    onMount() {
      document.body.classList.toggle(
        "無-dltweak__unmasked-word-highlight-enabled",
        settings.unmaskWords_highlight,
      );
    },
    shouldBeSkipped() {
      return !settings.unmaskWords;
    },
  },
  {
    id: KEYS.katakanaToHiragana,
    get caption() {
      return `Display kana titles in hiragana [${
        settings.katakanaToHiragana ? "enabled" : "disabled"
      }]`;
    },
    onClick() {
      settings.katakanaToHiragana = !settings.katakanaToHiragana;
      GM_setValue(KEYS.katakanaToHiragana, settings.katakanaToHiragana);
      registerMenuCommand(this);
    },
  },
  {
    id: KEYS.titleFormat,
    caption: "Change title format",
    onClick() {
      const value = prompt(
        "Enter new title format. Leave empty to reset. Available variables: $code, $maker, $title",
        settings.titleFormat,
      );
      if (value === null || value === settings.titleFormat) return;
      settings.titleFormat = value ? value : "[$code] [$maker] $title";
      GM_setValue(KEYS.titleFormat, settings.titleFormat);
    },
  },
];

export const registerSettings = (): void => registerMenuCommands(commands);
