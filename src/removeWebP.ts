import { getElements } from "./utils/getElements.ts";

function removeWebPElements(element: Element) {
  element
    .querySelectorAll("source[srcset$='.webp']")
    .forEach((e) => e.remove());
}

function productSliderMutationCallback(mutations: MutationRecord[]) {
  for (const mutation of mutations) {
    if (mutation.addedNodes.length !== 1) {
      return;
    }
    const node = mutation.addedNodes[0];
    if (
      node instanceof HTMLElement &&
      node.classList.contains("slider_popup_overlay")
    ) {
      removeWebPElements(node);
    }
  }
}

export function removeWebP(): void {
  const { productSlider } = getElements(
    { productSlider: ".product-slider" },
    false,
  );
  /** Translation page crash prevention. */
  if (!productSlider) return;
  removeWebPElements(productSlider);
  const observer = new MutationObserver(productSliderMutationCallback);
  observer.observe(productSlider, { childList: true });
}
