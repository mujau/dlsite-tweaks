# Dlsite Tweaks

## Features

- A setting to change preview images back to jpeg format (on by default).
- A setting to restore old tag names (on by default).
- A setting to unmask censored words in titles and descriptions (on by default).
- A button to toggle detailed list of the product's files.
- A button to toggle display of kana rendering of the product's title. Can be customized with a setting to display the titles in hiragana instead of original katakana (because I can't stand it; off by default).
- A button to copy the product's title. The format can be customized in userscript menu.
- A button linking to the product's details in JSON format.
- Automatic redirection of faulty `work` links to `announce`.

Settings can be changed from userscript menu.

## Instalation

The script is tested in the latest stable chromium with [violentmonkey](https://violentmonkey.github.io/).

To install, load `./dist/dlsite-tweaks.user.js` ([click](https://gitlab.com/mujau/dlsite-tweaks/-/raw/master/dist/dlsite-tweaks.user.js)) in your userscript manager.

## Building

The project is managed with [pnpm](https://pnpm.io/).

- Run `pnpm i` to install dependencies.
- Run `pnpm build` to build.
