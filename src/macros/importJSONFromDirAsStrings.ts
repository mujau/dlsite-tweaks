import { readdirSync, readFileSync } from "node:fs";
import path from "node:path";

/** Paths are resolved from root dir and I have no idea how to make them relative. */
export function importJSONFromDirAsStrings(
  dir: string,
): Record<string, string> {
  const obj: Record<string, string> = {};
  const files = readdirSync(dir, { withFileTypes: true });
  for (const file of files) {
    if (!file.isFile() || !file.name.endsWith(".json")) continue;
    const str = readFileSync(path.join(dir, file.name), "utf-8");
    obj[path.parse(file.name).name] = JSON.stringify(JSON.parse(str));
  }
  return obj;
}
