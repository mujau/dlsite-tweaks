# v4.3.0 2024-10-21

- Added a setting that converts kana titles from original katakana to hiragana.

# v4.2.0 2024-05-04

- Fixed crashing on translator unite pages.
- Disabled 強○ unmasking as the original word is ambiguous.
- Added more masked English words. The case is also preserved now.
- Pill padding sucked, so unmasked words are styled with simple colored background now. Also added a setting to disable highlighting altogether.
- Unmasking rules don't care about locale anymore.

# v4.1.0 2024-04-19

- Fixed setting labels not changing after click.
- Added Anime-Sharing search button.
- Added series title unmasking.
- Unmasked words are now styled as pills (used to be underline).
- Added more masked words (姦 family in Japanese and r\*pe in English).
- Japanese words are now unmasked in every locale.

# v4.0.1 2024-04-17

- Unmasked word's original value is now kept as title property and displayed on hover.
- Added ロリ to masked words.

# v4.0.0 2024-04-07

- Added a setting to unmask censored words (highlighted with pink underline; on by default).
- Added a setting to restore old tags (on by default).
- Added interface localization (Japanese and English for now).
- The format used by the clipboard button is now configurable.
- WebP removal can be now disabled (I thought it made images slower to load, but maybe it's just DLsite).
- Added settings to userscript menu.
- Removed uhtml dependency.
- The script is now compiled to IIFE.

# v3.2.0 2023-07-31

- Added JSON button to error pages.
- Removed debug logging from normal builds.
- Removed source mapping.
- Formatted the compiled script with prettier.
- Refactor.
