import { lang } from "../strings.ts";
import type { Product } from "../lib/DLsite/types.ts";
import { html } from "../utils/html.ts";

type Files = NonNullable<Product["contents"]>;

export function fileList(files: Files): HTMLElement {
  const list = html`
    <div id="無-dltweak__filelist">
      ${files.map(({ file_name, file_size, update_date }) => {
        const bytes = parseInt(file_size).toLocaleString(lang, {
          style: "unit",
          unit: "byte",
          unitDisplay: "long",
        });
        return html`<div class="無-dltweak__filelist-item">
          <div class="無-dltweak__filelist-title">${file_name}</div>
          <div class="無-dltweak__filelist-filesize">${bytes}</div>
          <div class="無-dltweak__filelist-updatedate">${update_date}</div>
        </div> `;
      })}
    </div>
  ` as HTMLDivElement;
  return list;
}
