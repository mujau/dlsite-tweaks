import { CreatorPrefix, ImagePath, Prefix, Site, WorkPrefix } from "./types.ts";
import { mergeRegex, splitAt } from "./utils.ts";

export const quickCodeRe = /[RBV][JGE]\d{4,8}/i;
const workLinkPrefixRe =
  /(?:https?:\/\/)?(?:www\.)?dlsite.com\/(?<site>[^\/]+)\/(?<ana>work|announce)\/=\/product_id\//i;
const workLinkPostfixRe = /(?:\.html)?/i;
export const workCodeRe =
  /(?<work_code>(?:RJ|RE|VJ|BJ)(?:\d{8}|\d{6}|\d{4}))(?!\d)/i;
export const creatorCodeRe =
  /(?<creator_code>(?:RG|VG|BG)(?:\d{8}|\d{4,5}))(?!\d)/i;
export const workLinkRe = mergeRegex([
  workLinkPrefixRe,
  workCodeRe,
  workLinkPostfixRe,
]);
export const genericCodeRe = mergeRegex([
  "(?:",
  workLinkPrefixRe,
  ")?",
  workCodeRe,
  workLinkPostfixRe,
  "|",
  creatorCodeRe,
]);

/** Best guess. Doesn't really matter anyway. */
export function guessSite(code: string): Site {
  const [prefix] = splitAt<Prefix>(code, 2);
  switch (prefix) {
    case "RJ":
    case "RG":
      return "maniax";
    case "RE":
      return "ecchi-eng";
    case "VJ":
    case "VG":
      return "pro";
    case "BJ":
    case "BG":
      return "books";
    default: {
      console.warn(`Unknown code prefix: \`${prefix}\``);
      return "home";
    }
  }
}

function getImagePath(prefix: WorkPrefix): ImagePath {
  switch (prefix) {
    case "RJ":
    case "RE":
      return "doujin";
    case "VJ":
      return "professional";
    case "BJ":
      return "books";
    default: {
      throw new Error(`Unknown code prefix: \`${prefix}\``);
    }
  }
}

function getBucket(prefix: WorkPrefix, no: string): string {
  const [a, b] = splitAt(no, -3);
  const bucket = `${+a + (b === "000" ? 0 : 1)}`.padStart(a.length, "0");
  return `${prefix}${bucket}000`;
}

export function getMainImageUrl(code: string, announce = false): string {
  const [prefix, no] = splitAt<WorkPrefix>(code, 2);
  const path = getImagePath(prefix);
  const bucket = getBucket(prefix, no);
  return announce
    ? `https://img.dlsite.jp/modpub/images2/ana/${path}/${bucket}/${code}_ana_img_main.jpg`
    : `https://img.dlsite.jp/modpub/images2/work/${path}/${bucket}/${code}_img_main.jpg`;
}

export function getWorkUrl(
  code: string,
  site = guessSite(code),
  announce = false,
): string {
  return announce
    ? `https://www.dlsite.com/${site}/announce/=/product_id/${code}.html`
    : `https://www.dlsite.com/${site}/work/=/product_id/${code}.html`;
}

export function getCreatorUrl(code: string, site = guessSite(code)): string {
  return `https://www.dlsite.com/${site}/circle/profile/=/maker_id/${code}.html`;
}

export function normalizeWorkCode(code: string): string {
  const [prefix, no] = splitAt<WorkPrefix>(code, 2);
  return prefix.toUpperCase() + no.padStart(6, "0");
}

export function normalizeCreatorCode(code: string): string {
  const [prefix, no] = splitAt<CreatorPrefix>(code, 2);
  return prefix.toUpperCase() + no.padStart(5, "0");
}
