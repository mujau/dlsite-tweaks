export type DLsiteCode = string;
export type WorkCode = DLsiteCode;
export type WorkPrefix = "RJ" | "RE" | "VJ" | "BJ";
export type CreatorPrefix = "RG" | "VG" | "BG";
export type Prefix = WorkPrefix | CreatorPrefix;
export type Site =
  | "home"
  | "maniax"
  | "ecchi-eng"
  | "pro"
  | "books"
  | "ai"
  | "aix";
export type Ana = "announce" | "work";
export type ImagePath = "doujin" | "professional" | "books";
export type Locale = "ja_JP" | "zh-TW" | "zh-CN" | "ko_KR" | "en_US";

export type WorkCodeGroups = {
  work_code: string | undefined;
};

export type WorkCodeLinkGroups = {
  site: Site | undefined;
  ana: Ana | undefined;
} & WorkCodeGroups;

export type CreatorCodeGroups = {
  creator_code: string | undefined;
};

/** General Audience | R15+ | R18+ */
type AgeCategory = 1 | 2 | 3;

interface WorkFile {
  "workno": WorkCode;
  "type": string;
  "file_name": string;
  "file_size": string;
  "file_size_unit": string;
  "width": string | null;
  "height": string | null;
  "hash": null;
  "display_mode": "";
  "update_date": string;
  "id": string;
  "upper(work_files.type)": Uppercase<WorkFile["type"]>;
  "extension": string;
}

interface ImageInfo extends WorkFile {
  relative_url: string;
  path_short: string;
  url: string;
  resize_url: string;
}

interface Content extends WorkFile {
  type: "content";
}

interface ContentWithUrl extends Content {
  url: string;
}

interface Trial extends WorkFile {
  type: "trial";
  relative_url: string;
  url: string;
}

interface WorkOption {
  id: string;
  options_id: string;
  value: string;
  name: string;
  name_en: string;
  display_sentence: string | null;
  display_sentence_en: string | null;
  category_id: string;
  category: string;
}

interface Creater {
  id: string;
  name: string;
  classification: string;
  sub_classification: string | null;
}

interface Genre {
  name: string;
  id: number;
  search_val: string;
  name_base: string;
}

interface Edition {
  workno: WorkCode;
  edition_id: number;
  edition_type: string;
  display_order: number;
  label: string;
}

/**
 * Not really complete.
 */
export interface Product {
  age_category: AgeCategory;
  age_category_string: string;
  books_id: string | null;
  brand_id: string | null;
  circle_id: string | null;
  cpu: string | null;
  directx: string | null;
  etc: string | null;
  file_type: string;
  file_type_string: string;
  hdd: string | null;
  intro: null;
  intro_s: string | null;
  label_id: string | null;
  label_name: string | null;
  machine: string | null;
  memory: string | null;
  pages: string | null;
  page_number: string | null;
  regist_date: string;
  series_id: string | null;
  series_name: string | null;
  vram: string | null;
  workno: WorkCode;
  work_name: string;
  work_name_kana: string | null;
  work_type: string;
  work_type_string: string;
  work_type_special: string | null;
  product_id: WorkCode;
  base_product_id: WorkCode;
  maker_id: string;
  maker_name: string;
  maker_name_en: string;
  alt_name: string;
  product_name: string;
  site_id: string;
  is_ana: boolean;
  work_category: string;
  platform: string[];
  work_parts: [];
  image_main: ImageInfo | null;
  image_thum: ImageInfo | null;
  image_thum_mini: ImageInfo | null;
  image_samples: ImageInfo[] | null;
  contents: Content[] | null;
  contents_with_url: ContentWithUrl[] | null;
  is_split_content: boolean;
  trials: Trial[] | null;
  work_options: Record<string, WorkOption>;
  /** Original spelling. `Record | Array` union isn't an error either. */
  creaters: Record<string, Creater> | [];
  genres: Genre[];
  editions: Edition[];
  update_date: string;
  product_dir: WorkCode;
}

export type productField = keyof Product;
