import { addApiButtonToErrorPage } from "./buttons.ts";
import { getMeta } from "./metadata.ts";

async function tryAnaRedirect() {
  const meta = getMeta();
  if (meta.is_ana) return false;
  const anaURL = location.href.replace("work", "announce");
  const response = await fetch(anaURL);
  if (response.ok) {
    console.debug("Redirecting to announce page");
    location.replace(anaURL);
    return true;
  }
  return false;
}

export async function handleErrorPage(): Promise<void> {
  const redirected = await tryAnaRedirect();
  if (redirected) return;
  addApiButtonToErrorPage();
}
