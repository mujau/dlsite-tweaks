// ==UserScript==
// @name        DLsite Tweaks
// @namespace   green-is-my-paddy
// @version     4.3.0
// @description Tweaks DLsite things.
// @author      mujau
// @homepageURL https://gitlab.com/mujau/dlsite-tweaks
// @supportURL  https://gitlab.com/mujau/dlsite-tweaks/-/issues
// @downloadURL https://gitlab.com/mujau/dlsite-tweaks/-/raw/master/dist/dlsite-tweaks.user.js
// @grant       GM_addStyle
// @grant       GM_getValue
// @grant       GM_setValue
// @grant       GM.registerMenuCommand
// @grant       GM.unregisterMenuCommand
// @grant       GM_setClipboard
// @run-at      document-end
// @match       *://www.dlsite.com/*/work/=/product_id/*
// @match       *://www.dlsite.com/*/announce/=/product_id/*
// ==/UserScript==

(function () {
  "use strict";

  /** Merges an array of strings and RegExp objects into a single RegExp. */
  function mergeRegex(parts, flags = "") {
    const patternList = [];
    const flagList = [];
    for (const part of parts) {
      if (part instanceof RegExp) {
        patternList.push(part.source);
        flagList.push(part.flags);
      } else patternList.push(part);
    }
    const mergedFlags = Array.from(
      flagList.reduce(
        (acc, flags) => {
          flags.split("").forEach((f) => acc.add(f));
          return acc;
        },
        new Set(flags.split("")),
      ),
    ).join("");
    return new RegExp(patternList.join(""), mergedFlags);
  }

  const workLinkPrefixRe =
    /(?:https?:\/\/)?(?:www\.)?dlsite.com\/(?<site>[^\/]+)\/(?<ana>work|announce)\/=\/product_id\//i;
  const workLinkPostfixRe = /(?:\.html)?/i;
  const workCodeRe =
    /(?<work_code>(?:RJ|RE|VJ|BJ)(?:\d{8}|\d{6}|\d{4}))(?!\d)/i;
  const creatorCodeRe = /(?<creator_code>(?:RG|VG|BG)(?:\d{8}|\d{4,5}))(?!\d)/i;
  const workLinkRe = mergeRegex([
    workLinkPrefixRe,
    workCodeRe,
    workLinkPostfixRe,
  ]);
  mergeRegex([
    "(?:",
    workLinkPrefixRe,
    ")?",
    workCodeRe,
    workLinkPostfixRe,
    "|",
    creatorCodeRe,
  ]);

  /**
   * Builds an url for querying DLsite's API for product details.
   * `locale` option defaults to `"ja_JP"` while `site` defaults to `"home"`.
   */
  function buildProductQueryURL(code, options) {
    const { fields, locale = "ja_JP", site = "home", ana } = options || {};
    const URL = `https://www.dlsite.com/${site}/api/=/product.json`;
    const searchParams = new URLSearchParams({ workno: code });
    if (fields) searchParams.append("fields", fields.join(","));
    if (locale) searchParams.append("locale", locale);
    if (ana) searchParams.append("is_ana", "1");
    return URL + "?" + searchParams.toString();
  }

  /* v1.1.0.0 2024-09-11 */
  class ElementNotFoundError extends Error {
    constructor(message, cause) {
      super(message, { cause });
      this.name = "ElementNotFoundError";
    }
  }
  const isArray = Array.isArray;
  /** DOM element binding helper. The input object describes wanted elements
   * either with selector strings or `[root, selector, isOptional?]` tuples. `root` can be
   * either a `ParentNode` or a string matching a (preceding) key in the object
   * for recursive selection. */
  function getElements(selectors, throwOnMissing = true) {
    const elems = {};
    for (const [key, val] of Object.entries(selectors)) {
      const [root, selector, isOptional] = isArray(val)
        ? val
        : [document, val, false];
      const rootNode = typeof root !== "string" ? root : elems[root];
      if (!(rootNode instanceof Node)) {
        if (rootNode === undefined) {
          throw new ReferenceError(`Root element \`${root}\` isn't defined`);
        } else if (rootNode === null) {
          if (!throwOnMissing) {
            elems[key] = null;
            continue;
          } else {
            throw new ReferenceError(`Root element \`${root}\` is null`);
          }
        } else {
          throw new Error("Something happened.");
        }
      }
      const maybeElem = rootNode.querySelector(selector);
      if (!maybeElem && throwOnMissing && !isOptional) {
        throw new ElementNotFoundError(`\`${key}\` not found`, key);
      }
      elems[key] = maybeElem;
    }
    return elems;
  }

  let meta;
  let metaFromAPI;
  function getMeta() {
    if (meta) return meta;
    const match = location.href.match(workLinkRe);
    if (!match) {
      throw new Error("The page URL couldn't be matched");
    }
    const { site, ana, work_code } = match.groups;
    const { title, makerName } = getElements(
      {
        title: "h1#work_name",
        makerName: "span.maker_name",
      },
      false,
    );
    return (meta = {
      site: site,
      code: work_code,
      is_ana: ana === "announce",
      title: title?.textContent.trim(), // https://github.com/microsoft/TypeScript/issues/10315
      maker: makerName?.textContent.trim(),
    });
  }
  const fields = ["work_name_kana", "contents"];
  async function getProduct() {
    const { code, site, is_ana } = meta;
    const headers = { "X-Requested-With": "XMLHttpRequest" };
    const query = buildProductQueryURL(code, {
      fields,
      site,
      ana: is_ana,
    });
    const response = await fetch(query, { headers });
    if (!response.ok) {
      throw new Error("Product metadata fetch failed");
    }
    const json = await response.json();
    if (!Array.isArray(json)) {
      throw new Error("API response is expected to be an array");
    }
    if (!json.length) {
      throw new Error("Empty API response");
    }
    if (json.length !== 1) {
      throw new Error("The API returned more than one result");
    }
    const product = json[0];
    return product;
  }
  async function getMetaFromAPI() {
    if (metaFromAPI) return metaFromAPI;
    return (metaFromAPI = await getProduct());
  }

  /* v0.0.2.0 2024-10-01 */
  function registerMenuCommand(command) {
    if (command.shouldBeSkipped?.()) return;
    command.onMount?.();
    GM.registerMenuCommand(command.caption, (e) => command.onClick(e), {
      id: command.id,
    });
  }
  function registerMenuCommands(commands) {
    for (const command of commands) {
      registerMenuCommand(command);
    }
  }

  const KEYS = {
    removeWebP: "REMOVE_WEBP",
    restoreTags: "RESTORE_TAGS",
    unmaskWords: "UNMASK_WORDS",
    unmaskWords_highlight: "UNMASK_WORDS__HIGHLIGHT",
    titleFormat: "TITLE_FORMAT",
    katakanaToHiragana: "KATAKANA_TO_HIRAGANA",
  };
  const settings = {
    removeWebP: GM_getValue(KEYS.removeWebP, true),
    restoreTags: GM_getValue(KEYS.restoreTags, true),
    unmaskWords: GM_getValue(KEYS.unmaskWords, true),
    unmaskWords_highlight: GM_getValue(KEYS.unmaskWords_highlight, true),
    titleFormat: GM_getValue(KEYS.titleFormat, "[$code] [$maker] $title"),
    katakanaToHiragana: GM_getValue(KEYS.katakanaToHiragana, false),
  };
  const commands = [
    {
      id: KEYS.removeWebP,
      get caption() {
        return `Remove WebP [${settings.removeWebP ? "enabled" : "disabled"}]`;
      },
      onClick() {
        settings.removeWebP = !settings.removeWebP;
        GM_setValue(KEYS.removeWebP, settings.removeWebP);
        registerMenuCommand(this);
      },
    },
    {
      id: KEYS.restoreTags,
      get caption() {
        return `Restore Tags [${settings.restoreTags ? "enabled" : "disabled"}]`;
      },
      onClick() {
        settings.restoreTags = !settings.restoreTags;
        GM_setValue(KEYS.restoreTags, settings.restoreTags);
        registerMenuCommand(this);
      },
    },
    {
      id: KEYS.unmaskWords,
      get caption() {
        return `Unmask censored words [${settings.unmaskWords ? "enabled" : "disabled"}]`;
      },
      onClick() {
        settings.unmaskWords = !settings.unmaskWords;
        GM_setValue(KEYS.unmaskWords, settings.unmaskWords);
        // Full rerender as unmaskWords_highlight depends on it.
        registerMenuCommands(commands);
      },
    },
    {
      id: KEYS.unmaskWords_highlight,
      get caption() {
        return `Highlight unmasked words [${settings.unmaskWords_highlight ? "enabled" : "disabled"}]`;
      },
      onClick() {
        settings.unmaskWords_highlight = !settings.unmaskWords_highlight;
        GM_setValue(KEYS.unmaskWords_highlight, settings.unmaskWords_highlight);
        registerMenuCommand(this);
      },
      onMount() {
        document.body.classList.toggle(
          "無-dltweak__unmasked-word-highlight-enabled",
          settings.unmaskWords_highlight,
        );
      },
      shouldBeSkipped() {
        return !settings.unmaskWords;
      },
    },
    {
      id: KEYS.katakanaToHiragana,
      get caption() {
        return `Display kana titles in hiragana [${settings.katakanaToHiragana ? "enabled" : "disabled"}]`;
      },
      onClick() {
        settings.katakanaToHiragana = !settings.katakanaToHiragana;
        GM_setValue(KEYS.katakanaToHiragana, settings.katakanaToHiragana);
        registerMenuCommand(this);
      },
    },
    {
      id: KEYS.titleFormat,
      caption: "Change title format",
      onClick() {
        const value = prompt(
          "Enter new title format. Leave empty to reset. Available variables: $code, $maker, $title",
          settings.titleFormat,
        );
        if (value === null || value === settings.titleFormat) return;
        settings.titleFormat = value ? value : "[$code] [$maker] $title";
        GM_setValue(KEYS.titleFormat, settings.titleFormat);
      },
    },
  ];
  const registerSettings = () => registerMenuCommands(commands);

  const filesize_map = {
    "ja-jp": "ファイル容量",
    "en-us": "File size",
    "zh-cn": "文件容量",
    "zh-tw": "檔案容量",
    "ko-kr": "파일 용량",
    "es-es": "Tamaño del Archivo",
    "de-de": "Dateigröße",
    "fr-fr": "Taille du fichier",
    "id-id": "Ukuran file",
    "it-it": "Dimensione del file",
    "pt-br": "Tamanho do arquivo",
    "sv-se": "Filstorlek",
    "th-th": "ขนาดไฟล์",
    "vi-vn": "Dung lượng tệp",
  };

  const i18n_map = {
    "en-us":
      '{"as_button__label":"A-S","api_button__label":"JSON","title_copy_button__label":"Clipboard","title_copy_button__title":"Copy work title to clipboard","filelist__show":"show details","filelist__hide":"hide details","kana__show":"show kana","kana__hide":"hide kana"}',
    "ja-jp":
      '{"as_button__label":"A-S","api_button__label":"JSON","title_copy_button__label":"クリップボード","title_copy_button__title":"作品名をクリップボードにコピーする","filelist__show":"詳細表示","filelist__hide":"詳細非表示","kana__show":"カナ表示","kana__hide":"カナ非表示"}',
  };
  const tag_map = {
    "en-us":
      '{"113":"Rape","114":"Coercion / Compulsion","115":"Reverse Rape","120":"Incest","121":"Gangbang","139":"Molestation","140":"Sexual Training","147":"Slave","151":"Captivity","154":"Fiendish / Brutal","157":"Hypnosis","163":"Bestiality","201":"Torture","207":"Loli","314":"Hypnotic Voice","326":"Brainwashing","525":"Mesugaki","531":"Lolibaba"}',
    "ja-jp":
      '{"113":"レイプ","114":"強制/無理矢理","115":"逆レイプ","120":"近親相姦","121":"輪姦","134":"陵辱","139":"痴漢","140":"調教","147":"奴隷","151":"監禁","154":"鬼畜","157":"催眠","163":"獣姦","164":"機械姦","201":"拷問","207":"ロリ","314":"催眠音声","324":"異種姦","326":"洗脳","465":"モブ姦","490":"蟲姦","495":"睡眠姦","525":"メスガキ","531":"ロリババア"}',
  };
  const word_map = {
    "en-us": '{"r*pe":"a","l*li":"o","g*ngbang":"a","sl*ve":"a"}',
    "ja-jp":
      '{"レ○プ":"イ","凌○":"辱","陵○":"辱","催○":"眠","奴○":"隷","メ○ガキ":"ス","痴○":"漢","獣○":"姦","輪○":"姦","蟲○":"姦","睡眠○":"姦","機械○":"姦","近親相○":"姦","○問":"拷","○す":"犯","○リ":"ロ"}',
  };
  const lang = document.documentElement.lang;
  const i18n =
    lang in i18n_map
      ? JSON.parse(i18n_map[lang])
      : JSON.parse(i18n_map["ja-jp"]);
  const alteredTags = lang in tag_map ? JSON.parse(tag_map[lang]) : null;
  const maskedWords = {
    ...JSON.parse(word_map["ja-jp"]),
    ...JSON.parse(word_map["en-us"]),
  };
  const filesizeString =
    lang in filesize_map ? filesize_map[lang] : filesize_map["ja-jp"];

  /* v1.1.2 2024-03-25 */
  function html(strings, ...subs) {
    const template = document.createElement("template");
    const arr = [strings[0]];
    subs.forEach((val, i) =>
      arr.push(
        `${typeof val === "string" ? val : `<param i=${i}>`}`,
        strings[i + 1],
      ),
    );
    template.innerHTML = arr.join("");
    template.content.querySelectorAll("param[i]").forEach((e) => {
      const sub = subs[+e.getAttribute("i")];
      sub instanceof Array ? e.replaceWith(...sub) : e.replaceWith(sub);
    });
    return template.content.children.length > 1
      ? template.content
      : template.content.children[0];
  }

  let toggle$1;
  let ruby;
  let workName;
  function katakanaToHiragana(str) {
    if (!str) return "";
    const buf = [];
    for (const c of str) {
      const codePoint = c.codePointAt(0);
      // From ァ to ヶ as ヷ ヸ ヹ ヺ are extremely obscure and converting them is not trivial.
      // http://www.rikai.com/library/kanjitables/kanji_codes.unicode.shtml
      const newCodePoint =
        codePoint < 12449 || codePoint > 12534 ? codePoint : codePoint - 96;
      buf.push(newCodePoint);
    }
    return String.fromCodePoint(...buf);
  }
  async function toggleClickHandler$1() {
    const { work_name_kana } = await getMetaFromAPI();
    if (toggle$1.classList.contains("無-dltweak__toggled")) {
      ruby.remove();
      toggle$1.classList.toggle("無-dltweak__toggled", false);
      toggle$1.textContent = `[${i18n.kana__show}]`;
    } else {
      const kana = settings.katakanaToHiragana
        ? katakanaToHiragana(work_name_kana)
        : work_name_kana || "";
      ruby = html`<rt>${kana}</rt>`;
      workName.prepend(ruby);
      toggle$1.classList.toggle("無-dltweak__toggled", true);
      toggle$1.textContent = `[${i18n.kana__hide}]`;
    }
  }
  /**
   * Current implementation is broken,
   * but it probably looks better than proper ruby.
   */
  function addRuby(target) {
    workName = target;
    toggle$1 = html`<a id="無-dltweak__kana-toggle">[${i18n.kana__show}]</a>`;
    target.append(toggle$1);
    toggle$1.addEventListener("click", toggleClickHandler$1);
  }

  function apiButton(site, code, is_ana) {
    const link = buildProductQueryURL(code, {
      locale: "",
      site,
      ana: is_ana,
    });
    const button = html`
      <div id="無-dltweak__api-button" class="無-dltweak__button">
        <a href=${link} rel="noreferrer" target="_blank">
          <span class="button_label">${i18n.api_button__label}</span>
        </a>
      </div>
    `;
    return button;
  }

  function asButton(code) {
    const link = `https://www.anime-sharing.com/search/?q=${code}`;
    const button = html`
      <div id="無-dltweak__as-button" class="無-dltweak__button">
        <a href=${link} rel="noreferrer" target="_blank">
          <span class="button_label">${i18n.as_button__label}</span>
        </a>
      </div>
    `;
    return button;
  }

  function titleCopyButton(code, maker, title) {
    const button = html`
      <div
        id="無-dltweak__title-copy-button"
        class="無-dltweak__button"
        title="${i18n.title_copy_button__title}"
      >
        <span class="button_label">${i18n.title_copy_button__label}</span>
      </div>
    `;
    button.addEventListener("click", () => {
      const format = settings.titleFormat;
      const formattedTitle = format
        .replaceAll("$code", code)
        .replaceAll("$maker", maker)
        .replaceAll("$title", title.trim());
      GM_setClipboard(formattedTitle);
    });
    return button;
  }

  function addButtons(target) {
    const { site, title, maker, code, is_ana } = getMeta();
    const buttons = [
      apiButton(site, code, is_ana),
      titleCopyButton(code, maker, title),
    ];
    if (!is_ana) buttons.unshift(asButton(code));
    target.after(...buttons);
  }
  function addApiButtonToErrorPage() {
    const { site, code, is_ana } = getMeta();
    const { wrapper } = getElements({ wrapper: "#wrapper" });
    wrapper.prepend(apiButton(site, code, is_ana));
  }

  async function tryAnaRedirect() {
    const meta = getMeta();
    if (meta.is_ana) return false;
    const anaURL = location.href.replace("work", "announce");
    const response = await fetch(anaURL);
    if (response.ok) {
      location.replace(anaURL);
      return true;
    }
    return false;
  }
  async function handleErrorPage() {
    const redirected = await tryAnaRedirect();
    if (redirected) return;
    addApiButtonToErrorPage();
  }

  function fileList(files) {
    const list = html`
      <div id="無-dltweak__filelist">
        ${files.map(({ file_name, file_size, update_date }) => {
          const bytes = parseInt(file_size).toLocaleString(lang, {
            style: "unit",
            unit: "byte",
            unitDisplay: "long",
          });
          return html`<div class="無-dltweak__filelist-item">
            <div class="無-dltweak__filelist-title">${file_name}</div>
            <div class="無-dltweak__filelist-filesize">${bytes}</div>
            <div class="無-dltweak__filelist-updatedate">${update_date}</div>
          </div> `;
        })}
      </div>
    `;
    return list;
  }

  let toggle;
  let list;
  let workOutline;
  async function toggleClickHandler() {
    if (toggle.classList.contains("無-dltweak__toggled")) {
      list.remove();
      toggle.classList.toggle("無-dltweak__toggled", false);
      toggle.textContent = `[${i18n.filelist__show}]`;
      return;
    }
    const { contents: files } = await getMetaFromAPI();
    if (!list) {
      if (!files || !files.length) {
        list = html`<div>僕には何もない。</div>`;
      } else {
        list = fileList(files);
      }
    }
    workOutline.after(list);
    toggle.classList.toggle("無-dltweak__toggled", true);
    toggle.textContent = `[${i18n.filelist__hide}]`;
  }
  function getElementByXpath(path, context = document) {
    return document.evaluate(
      path,
      context,
      null,
      XPathResult.FIRST_ORDERED_NODE_TYPE,
      null,
    ).singleNodeValue;
  }
  function addFileInfo() {
    const meta = getMeta();
    if (meta.is_ana) {
      return;
    }
    ({ workOutline } = getElements({ workOutline: "#work_outline" }));
    const xpath = `//th[.='${filesizeString}']/following-sibling::td//div[@class='main_genre']`;
    const filesizeContainer = getElementByXpath(xpath, workOutline);
    if (!filesizeContainer) {
      console.warn("Failed to match filesize row");
      return;
    }
    toggle = html`<a id="無-dltweak__filelist-toggle"
      >[${i18n.filelist__show}]</a
    >`;
    filesizeContainer.append(toggle);
    toggle.addEventListener("click", toggleClickHandler);
  }

  function removeWebPElements(element) {
    element
      .querySelectorAll("source[srcset$='.webp']")
      .forEach((e) => e.remove());
  }
  function productSliderMutationCallback(mutations) {
    for (const mutation of mutations) {
      if (mutation.addedNodes.length !== 1) {
        return;
      }
      const node = mutation.addedNodes[0];
      if (
        node instanceof HTMLElement &&
        node.classList.contains("slider_popup_overlay")
      ) {
        removeWebPElements(node);
      }
    }
  }
  function removeWebP() {
    const { productSlider } = getElements(
      { productSlider: ".product-slider" },
      false,
    );
    /** Translation page crash prevention. */
    if (!productSlider) return;
    removeWebPElements(productSlider);
    const observer = new MutationObserver(productSliderMutationCallback);
    observer.observe(productSlider, { childList: true });
  }

  const re$1 = /genre\/(\d+)/;
  function restoreTags() {
    if (!alteredTags) return;
    const tags = document.querySelectorAll(".main_genre > a[href*='genre']");
    for (const tag of tags) {
      const match = tag.href.match(re$1);
      if (!match) continue;
      const originalTag = alteredTags[match[1]];
      if (!originalTag) continue;
      if (tag.innerText !== originalTag) tag.innerText = originalTag;
    }
  }

  const style =
    ':root{--無__dlsite-link:#039;--無__dlsite-text-paleblue:#536280;--無__dlsite-work_buy-background:#e6eaf2;--無__dlsite-work_buy-border:#b7bed0;--無__dlsite-pink:#fa6496;}.base_title_br{display:grid;grid-template-rows:auto auto;grid-template-columns:3fr;align-items:end;}div.base_title_br h1{white-space:normal;margin-right:80px;word-break:break-word;}.base_title_br .icon_wrap{grid-column:span 100;}.base_title_br>*,.base_title_br .link_share{position:static;}rt{display:block;font-size:11px;}#work_name{user-select:auto !important;}h1#work_name>a#無-dltweak__kana-toggle{all:unset;margin-left:5px;font-size:11px;vertical-align:super;}h1#work_name>a#無-dltweak__kana-toggle,#無-dltweak__filelist-toggle{color:var(--無__dlsite-link);cursor:pointer;}h1#work_name>a#無-dltweak__kana-toggle:hover,#無-dltweak__filelist-toggle:hover{text-decoration:underline;}.無-dltweak__button,.link_dl_ch,.link_share{margin-bottom:2px;}.無-dltweak__button{position:relative;display:flex;width:fit-content;height:20px;border-radius:2px;margin-left:5px;background-color:#4baef3;font-size:11px;cursor:pointer;color:#fff;float:right;}.無-dltweak__button a{color:#fff;}.無-dltweak__button>*{display:flex;align-items:center;padding:0 8px;}.無-dltweak__button .button_label::before{padding-right:4px;font-family:"Font Awesome 5 pro";font-weight:100;}#無-dltweak__title-copy-button{background-color:#38ad3f;}#無-dltweak__title-copy-button .button_label::before{content:"\\f0c5";}#無-dltweak__api-button{background-color:#12c9cb;}#無-dltweak__as-button{background-color:#2196f3;}#無-dltweak__as-button .button_label::before{content:"\\f002";font-size:10px;}table#work_outline{margin-bottom:5px;}#無-dltweak__filelist{display:grid;gap:4px;grid-template-columns:repeat(auto-fill,minmax(145px,max-content));color:var(--無__dlsite-text-paleblue);}.無-dltweak__filelist-item{padding:4px;background-color:var(--無__dlsite-work_buy-background);border:1px solid var(--無__dlsite-work_buy-border);border-radius:2px;}.無-dltweak__filelist-title{font-weight:bold;}body.無-dltweak__unmasked-word-highlight-enabled .無-dltweak__unmasked-word{display:inline-flex;max-height:1.5em;background-color:var(--無__dlsite-pink);color:#fff;align-items:center;}';

  /* v1.1.0 2024-04-11 */
  /** Executes a regex search on a text node and replaces the matched range.
   * @returns the replacement in case of a match and `null` otherwise. */
  function replaceTextNode(node, regex, replacer) {
    if (!node.data.trim()) return null;
    const match = regex.exec(node.data);
    regex.lastIndex = 0;
    if (!match) return null;
    const replacement = replacer(match);
    node = node.splitText(match.index);
    node.splitText(match[0].length);
    node.replaceWith(replacement);
    return replacement;
  }
  /** Executes a regex search on a document tree and replaces matched text
   * in-place.
   * @returns an array of replacements. */
  function replaceText(
    root,
    regex,
    replacer,
    { tagsToExclude, classesToExclude, filter } = {},
  ) {
    const replacements = [];
    const excludedTagSet =
      tagsToExclude?.reduce(
        (acc, val) => acc.add(val.toUpperCase()),
        new Set(),
      ) ?? null;
    const selector = classesToExclude
      ? "." + classesToExclude.join(", .")
      : null;
    const reject = (node) =>
      !!(
        excludedTagSet?.has(node.tagName) ||
        (selector && node.matches(selector)) ||
        filter?.(node)
      );
    const stack = [root.firstChild];
    while (stack.length) {
      let node = stack.pop();
      while (node) {
        switch (node.nodeType) {
          case Node.ELEMENT_NODE: {
            if (!reject(node)) {
              if (node.nextSibling) stack.push(node.nextSibling);
              node = node.firstChild;
              continue;
            }
            break;
          }
          case Node.TEXT_NODE: {
            const replacement = replaceTextNode(node, regex, replacer);
            if (replacement) {
              replacements.push(replacement);
              node = replacement;
            }
            break;
          }
        }
        node = node.nextSibling;
      }
    }
    return replacements;
  }

  const enabled = settings.unmaskWords || undefined;
  const re =
    enabled &&
    new RegExp(
      `(${Object.keys(maskedWords)
        .map((m) => m.replaceAll("*", "\\*"))
        .join(")|(")})`,
      "gi",
    );
  const maskRe = /○|\*/;
  const maskedCharByIndex = Object.values(maskedWords);
  function unmaskedWordFromGroups(...arr) {
    const word = arr.shift();
    const char = maskedCharByIndex[arr.findIndex((e) => !!e)];
    /** No Japanese masking or lowercase letters. */
    const capitalize = /^[^○\p{Ll}]+$/u.test(word);
    return word.replace(maskRe, capitalize ? char.toUpperCase() : char);
  }
  function unmask(target) {
    replaceText(target, re, (arr) => {
      return html`<span class="無-dltweak__unmasked-word" title=${arr[0]}
        >${unmaskedWordFromGroups(...arr)}</span
      >`;
    });
  }
  function unmaskWords(workName) {
    const { workParts, series } = getElements({
      workParts: ".work_parts_container",
      series: [document, "a[href*='title_d']", true],
    });
    if (maskRe.test(workName.textContent)) {
      unmask(workName);
      document.title = document.title.replaceAll(re, unmaskedWordFromGroups);
    }
    if (maskRe.test(workParts.textContent)) {
      unmask(workParts);
    }
    if (series && maskRe.test(series.textContent)) {
      unmask(series);
    }
  }

  function isErrorPage() {
    return !!document.querySelector(".error_box");
  }
  void (function init() {
    GM_addStyle(style);
    if (isErrorPage()) {
      handleErrorPage();
      return;
    }
    registerSettings();
    const { workName } = getElements({ workName: "h1#work_name" });
    if (settings.removeWebP) removeWebP();
    if (settings.restoreTags) restoreTags();
    if (settings.unmaskWords) unmaskWords(workName);
    addButtons(workName);
    addRuby(workName);
    addFileInfo();
  })();
})();
