import pkg from "./package.json" with { type: "json" };

export default `// ==UserScript==
// @name        ${pkg.productName}
// @namespace   green-is-my-paddy
// @version     ${pkg.version}
// @description ${pkg.description}
// @author      ${pkg.author}
// @homepageURL https://gitlab.com/mujau/dlsite-tweaks
// @supportURL  https://gitlab.com/mujau/dlsite-tweaks/-/issues
// @downloadURL https://gitlab.com/mujau/dlsite-tweaks/-/raw/master/dist/dlsite-tweaks.user.js
// @grant       GM_addStyle
// @grant       GM_getValue
// @grant       GM_setValue
// @grant       GM.registerMenuCommand
// @grant       GM.unregisterMenuCommand
// @grant       GM_setClipboard
// @run-at      document-end
// @match       *://www.dlsite.com/*/work/=/product_id/*
// @match       *://www.dlsite.com/*/announce/=/product_id/*
// ==/UserScript==\n`;
