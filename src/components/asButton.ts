import { i18n } from "../strings.ts";
import { html } from "../utils/html.ts";

export function asButton(code: string): HTMLElement {
  const link = `https://www.anime-sharing.com/search/?q=${code}`;
  const button = html`
    <div id="無-dltweak__as-button" class="無-dltweak__button">
      <a href=${link} rel="noreferrer" target="_blank">
        <span class="button_label">${i18n.as_button__label}</span>
      </a>
    </div>
  ` as HTMLDivElement;
  return button;
}
