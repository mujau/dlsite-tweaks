/** Merges an array of strings and RegExp objects into a single RegExp. */
export function mergeRegex(parts: (string | RegExp)[], flags = ""): RegExp {
  const patternList = [];
  const flagList = [];
  for (const part of parts) {
    if (part instanceof RegExp) {
      patternList.push(part.source);
      flagList.push(part.flags);
    } else patternList.push(part);
  }
  const mergedFlags = Array.from(
    flagList.reduce(
      (acc, flags) => {
        flags.split("").forEach((f) => acc.add(f));
        return acc;
      },
      new Set(flags.split("")),
    ),
  ).join("");
  return new RegExp(patternList.join(""), mergedFlags);
}

/** Splits a string at the given index and returns both parts. The parts are
 * ordered from the left even if index is negative. */
export function splitAt<A = string, B = string>(
  str: string,
  index: number,
): [A, B] {
  return [str.slice(0, index), str.slice(index)] as [A, B];
}
