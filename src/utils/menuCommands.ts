/* v0.0.2.0 2024-10-01 */

export type MenuCommand<T extends string = string> = {
  id: T;
  caption: string;
  onClick: (event: MouseEvent | KeyboardEvent) => void;
  onMount?: () => void;
  shouldBeSkipped?: () => boolean;
};

export function registerMenuCommand(command: MenuCommand): void {
  if (command.shouldBeSkipped?.()) return;
  command.onMount?.();
  GM.registerMenuCommand(command.caption, (e) => command.onClick(e), {
    id: command.id,
  });
}

export function unregisterMenuCommand(command: MenuCommand): void {
  GM.unregisterMenuCommand(command.id);
}

export function registerMenuCommands(commands: MenuCommand[]): void {
  for (const command of commands) {
    registerMenuCommand(command);
  }
}
