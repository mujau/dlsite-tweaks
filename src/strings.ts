import { importJSONFromDirAsStrings } from "./macros/importJSONFromDirAsStrings.ts" with { type: "macro" };
import filesize_map from "../strings/filesize.json" with { type: "json" };
import i18n_schema from "../strings/i18n/ja-jp.json" with { type: "json" };

const i18n_map = importJSONFromDirAsStrings("./strings/i18n");
const tag_map = importJSONFromDirAsStrings("./strings/altered_tags");
const word_map = importJSONFromDirAsStrings("./strings/masked_words");

export const lang = document.documentElement.lang;

export const i18n = (
  lang in i18n_map
    ? JSON.parse(i18n_map[lang]!)
    : JSON.parse(i18n_map["ja-jp"]!)
) as typeof i18n_schema;

export const alteredTags =
  lang in tag_map
    ? (JSON.parse(tag_map[lang]!) as Record<string, string>)
    : null;

export const maskedWords = {
  ...JSON.parse(word_map["ja-jp"]!),
  ...JSON.parse(word_map["en-us"]!),
} as Record<string, string>;

export const filesizeString =
  lang in filesize_map
    ? filesize_map[lang as keyof typeof filesize_map]
    : filesize_map["ja-jp"];
