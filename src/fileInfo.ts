import { fileList } from "./components/fileList.ts";
import { getMeta, getMetaFromAPI } from "./metadata.ts";
import { filesizeString, i18n } from "./strings.ts";
import { getElements } from "./utils/getElements.ts";
import { html } from "./utils/html.ts";

let toggle: HTMLElement;
let list: HTMLElement;
let workOutline: HTMLElement;

async function toggleClickHandler() {
  if (toggle.classList.contains("無-dltweak__toggled")) {
    list.remove();
    toggle.classList.toggle("無-dltweak__toggled", false);
    toggle.textContent = `[${i18n.filelist__show}]`;
    return;
  }

  const { contents: files } = await getMetaFromAPI();
  if (!list) {
    if (!files || !files.length) {
      list = html`<div>僕には何もない。</div>` as HTMLElement;
    } else {
      list = fileList(files);
    }
  }
  workOutline.after(list);
  toggle.classList.toggle("無-dltweak__toggled", true);
  toggle.textContent = `[${i18n.filelist__hide}]`;
}

function getElementByXpath(path: string, context: Node = document) {
  return document.evaluate(
    path,
    context,
    null,
    XPathResult.FIRST_ORDERED_NODE_TYPE,
    null,
  ).singleNodeValue;
}

export function addFileInfo(): void {
  const meta = getMeta();
  if (meta.is_ana) {
    return;
  }

  ({ workOutline } = getElements({ workOutline: "#work_outline" }));
  const xpath = `//th[.='${filesizeString}']/following-sibling::td//div[@class='main_genre']`;
  const filesizeContainer = getElementByXpath(
    xpath,
    workOutline,
  ) as HTMLElement;
  if (!filesizeContainer) {
    console.warn("Failed to match filesize row");
    return;
  }

  toggle = html`<a id="無-dltweak__filelist-toggle"
    >[${i18n.filelist__show}]</a
  >` as HTMLAnchorElement;
  filesizeContainer.append(toggle);
  toggle.addEventListener("click", toggleClickHandler);
}
