import type { Locale, productField, Site, WorkCode } from "./types.ts";

export interface Options {
  fields?: readonly productField[];
  locale?: Locale | "" | undefined;
  site?: Site;
  ana?: boolean;
}

/**
 * Builds an url for querying DLsite's API for product details.
 * `locale` option defaults to `"ja_JP"` while `site` defaults to `"home"`.
 */
export function buildProductQueryURL(
  code: WorkCode,
  options?: Options,
): string {
  const { fields, locale = "ja_JP", site = "home", ana } = options || {};
  const URL = `https://www.dlsite.com/${site}/api/=/product.json`;
  const searchParams = new URLSearchParams({ workno: code });
  if (fields) searchParams.append("fields", fields.join(","));
  if (locale) searchParams.append("locale", locale);
  if (ana) searchParams.append("is_ana", "1");
  return URL + "?" + searchParams.toString();
}
