import { getMetaFromAPI } from "./metadata.ts";
import { settings } from "./settings.ts";
import { i18n } from "./strings.ts";
import { html } from "./utils/html.ts";

let toggle: HTMLElement;
let ruby: HTMLElement;
let workName: HTMLElement;

function katakanaToHiragana(str: string | null) {
  if (!str) return "";
  const buf: number[] = [];
  for (const c of str) {
    const codePoint = c.codePointAt(0)!;
    // From ァ to ヶ as ヷ ヸ ヹ ヺ are extremely obscure and converting them is not trivial.
    // http://www.rikai.com/library/kanjitables/kanji_codes.unicode.shtml
    const newCodePoint = codePoint < 12449 || codePoint > 12534
      ? codePoint
      : codePoint - 96;
    buf.push(newCodePoint);
  }
  return String.fromCodePoint(...buf);
}

async function toggleClickHandler() {
  const { work_name_kana } = await getMetaFromAPI();
  if (toggle.classList.contains("無-dltweak__toggled")) {
    ruby.remove();
    toggle.classList.toggle("無-dltweak__toggled", false);
    toggle.textContent = `[${i18n.kana__show}]`;
  } else {
    const kana = settings.katakanaToHiragana
      ? katakanaToHiragana(work_name_kana)
      : work_name_kana || "";
    ruby = html`<rt>${kana}</rt>` as HTMLElement;
    workName.prepend(ruby);
    toggle.classList.toggle("無-dltweak__toggled", true);
    toggle.textContent = `[${i18n.kana__hide}]`;
  }
}

/**
 * Current implementation is broken,
 * but it probably looks better than proper ruby.
 */
export function addRuby(target: HTMLElement): void {
  workName = target;
  toggle = html`<a id="無-dltweak__kana-toggle"
    >[${i18n.kana__show}]</a
  >` as HTMLElement;
  target.append(toggle);
  toggle.addEventListener("click", toggleClickHandler);
}
