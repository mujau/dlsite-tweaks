import { settings } from "../settings.ts";
import { i18n } from "../strings.ts";
import { html } from "../utils/html.ts";

export function titleCopyButton(
  code: string,
  maker: string,
  title: string,
): HTMLElement {
  const button = html`
    <div
      id="無-dltweak__title-copy-button"
      class="無-dltweak__button"
      title="${i18n.title_copy_button__title}"
    >
      <span class="button_label">${i18n.title_copy_button__label}</span>
    </div>
  ` as HTMLDivElement;
  button.addEventListener("click", () => {
    const format = settings.titleFormat;
    const formattedTitle = format
      .replaceAll("$code", code)
      .replaceAll("$maker", maker)
      .replaceAll("$title", title.trim());
    GM_setClipboard(formattedTitle);
  });
  return button;
}
