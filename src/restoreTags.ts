import { alteredTags } from "./strings.ts";

const re = /genre\/(\d+)/;

export function restoreTags(): void {
  if (!alteredTags) return;
  const tags = document.querySelectorAll(".main_genre > a[href*='genre']");
  for (const tag of tags) {
    const match = tag.href.match(re);
    if (!match) continue;
    const originalTag = alteredTags[match[1]!];
    if (!originalTag) continue;
    if (tag.innerText !== originalTag) tag.innerText = originalTag;
  }
}
