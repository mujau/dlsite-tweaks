/* v1.1.0.0 2024-09-11 */

import type { ParseSelector } from "typed-query-selector/parser";

type NullableMappedType<T> = {
  [K in keyof T]: T[K] | null;
};

export type ElementSelectors = {
  [key: string]:
    | string
    | readonly [
      root: ParentNode | string,
      selector: string,
      isOptional?: boolean,
    ];
};

export type Elements<T extends ElementSelectors> = {
  [key in keyof T]: T[key] extends string ? ParseSelector<T[key], HTMLElement>
    : T[key][2] extends true ? ParseSelector<T[key][1], HTMLElement> | null
    : ParseSelector<T[key][1], HTMLElement>;
};

export type NullableElements<T extends ElementSelectors> = NullableMappedType<
  Elements<T>
>;

export class ElementNotFoundError extends Error {
  constructor(message: string, cause: string) {
    super(message, { cause });
    this.name = "ElementNotFoundError";
  }
}

// https://github.com/microsoft/TypeScript/issues/17002
type IsArray = (arg: unknown) => arg is unknown[] | readonly unknown[];
const isArray = Array.isArray as IsArray;

/** DOM element binding helper. The input object describes wanted elements
 * either with selector strings or `[root, selector, isOptional?]` tuples. `root` can be
 * either a `ParentNode` or a string matching a (preceding) key in the object
 * for recursive selection. */
export function getElements<
  S extends ElementSelectors,
  T extends boolean = true,
>(
  selectors: S,
  throwOnMissing = true as T, // https://github.com/microsoft/TypeScript/issues/56315
): T extends true ? Elements<S> : NullableElements<S> {
  const elems: Record<string, Element | null> = {};
  for (const [key, val] of Object.entries(selectors)) {
    const [root, selector, isOptional] = isArray(val)
      ? val
      : [document, val, false];
    const rootNode = typeof root !== "string" ? root : elems[root];
    if (!(rootNode instanceof Node)) {
      if (rootNode === undefined) {
        throw new ReferenceError(`Root element \`${root}\` isn't defined`);
      } else if (rootNode === null) {
        if (!throwOnMissing) {
          elems[key] = null;
          continue;
        } else {
          throw new ReferenceError(`Root element \`${root}\` is null`);
        }
      } else {
        throw new Error("Something happened.");
      }
    }
    const maybeElem = rootNode.querySelector(selector);
    if (!maybeElem && throwOnMissing && !isOptional) {
      throw new ElementNotFoundError(`\`${key}\` not found`, key);
    }
    elems[key] = maybeElem;
  }
  return elems as T extends true ? Elements<S> : NullableElements<S>;
}
